package module1;

public class Ejercicio3 {
	
	public static void main(String[] args) {
		System.out.println("Tecla de escape\t\t\tSignificado\n");
		System.out.println("\\n\t\t\t\tNueva L�nea");
		System.out.println("\\t\t\t\t\tTabulaci�n de espacios");
		System.out.println("\\� \t\t\t\tComillas dobles \"hola\"");
		System.out.println("\\\\ \t\t\t\tLa barra dentro del texto \\hola\\");
		System.out.println("\\�\t\t\t\tComilla simple holan\'t");
	}
}
