package module2;

import java.util.Scanner;

public class Exercise_22 {

	
	//Inicializar la función de esperar x cantidad de milisegundos previamente usada 
	public static void wait(int ms){
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }
	
	
	public static void main(String[] args) {
			
		Scanner s = new Scanner(System.in);
		System.out.println("Ingrese la cantidad de empleados que quiere randomizar");
		
		//Inicializar la variable que contenerá la cantidad de empleados a randomizar
		int owo = s.nextInt();
		
		/*La idea del codigo es la siguiente: lo unico que yo necesito es randomizar las entradas de cada una 
		 * de las opciones, el unico problema es la categoría que es un array de chars y no de numeros, por lo tanto
		 * inicializo una variable que solo pueda existir de 0 a 2 y luego la asigno a al indice de mi nueva verdadera 
		 * variable, por lo tanto si mi indice random es 2, la nueva variable de categoría va a ser 
		 * variable_nueva = array_de_categorías[numero_random(indice_random)], por lo tanto de esta manera asigno una categoría
		 * random a cada uno de los empleados
		 */
		
		
		int category_index = 0; //Variable de indice de random 
		
		char selected_category = 'a'; //Variable de categoría utilizable 
		
		int selected_antiquity =0; //Variable de antigüedad
		
		int selected_income =0; //Variable de ingreso
		
		int reward_antiquity = 30; //Siempre que la antiguedad no sea de 1 a 10, la recompenza por antigüedad es de 30%,por lo tanto lo asigno como mi valor default.
		
		int reward_category = 0; //Declaro la variable de la ganancia por categoría 
		
		
		//Declaro los 4 arrays 1 x n (donde n es el largo de los mismos) que luego serán usados para 
		//comparar los valores seleccionados.
		
		int[] category_reward= {1000,2000,3000};
		int[] antiquity_reward= {5,5,5,5,5,10,10,10,10,10};
		int[] antiquity_correspondence= {1,2,3,4,5,6,7,8,9,10};
		char[] category_correspondence = {'a','b','c'}; 
		
		
		
		for(int z = 0; z<owo;z++) {
		//Utilizo en for loop para repetir el programa tantas veces como el usuario quiera (con la variable owo)	
			
			
			//Estas tres variables son redeclaradas para que el while loop se vuelva a ejecutar con cada ciclo del for
			//loop tal y como es pedido en el ejercicio
			
			category_index = -1; //Necesito iniciar esta variable como -1 para que los valores siempre esten entre 0 y 1 
			selected_antiquity = 0; //Necesito iniciar esta variable como 0 por que la antigüedad no puede ser 0  
			selected_income = 0; //Necesito iniciar esta variable como 0 por que el ingreso nunca puede ser 0 
			
			while (category_index<0 || category_index>2 || selected_antiquity<0 || selected_antiquity>70 || selected_income<0) {
			//Siempre y cuando los valores no sean correctos, ejecuta este while loop	 
				
				category_index = (int)(Math.random()*100); //Randomizar las tres entradas
				selected_antiquity = (int)(Math.random()*100);
				selected_income = (int)(Math.random()*100000);
				}

			/*Ahora que los valores ya fueron correctamente seleccionados
			 * solo necesitamos asignar nuestro indice random al array de categorías random
			 * y luego ejecutar el mismo codigo del ejercicio anterior
			 */
			
			selected_category = category_correspondence[category_index];

			
			//Determinar la posición de la antigüedad
			for (int i = 0; i<antiquity_reward.length; i++) {
				if (selected_antiquity==antiquity_correspondence[i]) {
					reward_antiquity = antiquity_reward[i];  
					
	 			}
			}
			
			
			//Determinar la posición de la categoría
			for (int x = 0; x<category_reward.length; x++) {
				if (selected_category==category_correspondence[x]) {
					reward_category = category_reward[x];  
	 			}
			}
			
			
			//Calcular porcentaje
			double perc = (((double) reward_antiquity) / (100))*(selected_income + reward_category);
			double total = (selected_income + reward_category) + perc;
			
			
			//Informarlo
			System.out.println("El ingreso neto del empleado Nº" +(z+1)+ " será: " +total+ "$");
			System.out.println("Al sueldo neto del empleado Nº" +(z+1)+ " se le agregarán: " +reward_category+ "$ por categoría ("+selected_category+")");
			System.out.println("Un " +reward_antiquity+ "% del sueldo total (" +perc+ ") será de la antigüedad, la antiguedad del empleado Nº"+(z+1)+" son "+selected_antiquity+" años. \n");
			
			wait(500);
			
		
		}
		
		
		
	}

}
