package module2;
//Ingresar las libs de manejo de matrices y escaneo
import java.util.Arrays;
import java.util.Scanner;

public class Exercise_7 {
	public static void main(String[] args) {
		System.out.println("Ingrese la cantidad de valores que quiere comparar");
		
		double[] numbers;
		
		Scanner s = new Scanner(System.in);
		
		int x = s.nextInt(); 
		
		numbers = new double[x];
		
		for(int i = 0; i<x; i++) {
			System.out.print("Ingresar numero Nº"+ (i+1) +":");
			numbers[i] = s.nextDouble();
		}
		
		double max = Arrays.stream(numbers).max().getAsDouble();
		System.out.println("El valor mas grande es: "+max);
		
	}
}
