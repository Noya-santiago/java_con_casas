package module2;
//Inicializar la librería de escaneo de numeros
import java.util.Scanner;

public class Exercise_4 {
	public static void main(String[] args) {
		/*
		Este programa utiliza exactamente la misma lógica 
		que el anterior, por lo tanto es basicamente 
		un copypaste xd
		*/
		System.out.println("Ingrese la categoría");
		//Inicializar dos arrays de R= 1x3 indicando la categoría y otro indicando su homologo	
		String[] Category = {"Hijo", "Padres", "Abuelos"};
		String[] Category_letter = {"a", "b", "c"};
		
		Scanner s = new Scanner(System.in);
	
		//Ingresar variable de error en el ingreso
		int error = 0;
		
		String Selected_Category = s.nextLine();
		
		for (int i = 0; i<Category.length; i++ ) {
			if (Selected_Category.equals(Category_letter[i])) {
				System.out.println("La categoría seleccionada es: "+Category_letter[i]+ ", y la palabra a mostrar es: " +Category[i]);			
				error++; 
			}	
		}
		//Resolución de errores 
		if (error == 0) {
			System.out.println("Ha ingresado la categoría mal");
		}
		
	}
}
