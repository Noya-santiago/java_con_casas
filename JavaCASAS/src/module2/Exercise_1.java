package module2;

//importar librería de manejo de matrices y escaneo de numeros
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.*;

public class Exercise_1 {
	private static Scanner s;

	public static void main(String[] args) {
		
		//Inicializar la mariz que contenerá las notas del alumno
		double grades[];
		
		s = new Scanner(System.in);
		
		//Almacenar matriz en la memoria agregándole la cantidad de notas del alumno
		grades = new double[3];
		
		//Inicializar loop para cargar los valores en el array 
		for (int i = 0; i < grades.length; i++) {
			System.out.print("Ingresar nota Nº"+ (i+1) +":");
			grades[i] = s.nextDouble();
			
			//Resolución de errores
			if (grades[i] > 10 || grades[i] < 0){
				System.out.print("Las notas ingresadas no son correctas, reinicie el programa");
				System.exit(0);
		}
		
		}
		
		//Inicializar la condición en la cual el alumno esta desaprobado:
		//i.e. la suma de los valores del array / el largo del mismo 
		if (((DoubleStream.of(grades).sum())/grades.length)>=7) {
			System.out.println("El alumno está aprobado, las notas son:" + Arrays.toString(grades));
		}
		else {
			System.out.println("El alumno está desaprobado, las notas son:" + Arrays.toString(grades));
		}			
	}
}
