package module2;

import java.util.Scanner;

public class Exercise_17 {
	public static void main(String[] args) {
		System.out.println("Ingrese la variable: ");
		Scanner s = new Scanner(System.in);
		int If_sum = 0;
		int ifnt_sum = 0; 
		int number = s.nextInt();
		int accum = 0; 
		
		for (int i=0;i<10; i++) {
			accum += number;
			//Con if 
			if ((accum%2)==0) {
				If_sum += accum; 
			}
			//Sin if 
			while ((accum%2)==0) {
				ifnt_sum += accum;
				break;
			}
			
		}
	
		System.out.println("la suma de las tablas usando If es =" + If_sum);
		System.out.println("la suma de las tablas sin usar If es =" + ifnt_sum);
	}
}