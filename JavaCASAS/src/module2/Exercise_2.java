package module2;

//ingresar librería de input de numeros
import java.util.Scanner;

public class Exercise_2 {
	public static void main(String[] args) {
	System.out.println("Ingrese el numero que quiere saber si es par o impar: ");
	Scanner s = new Scanner(System.in);
	int number = s.nextInt();
	//preguntar el resto del ejercicio, si este es 0 el numero es par 
	if((number%2)==0) {
		System.out.println("El número ingresado ("+number+"), aunque no pueda creerlo, es par.");
		}else{
		System.out.println("El número ingresado ("+number+"), aunque no pueda creerlo, es impar.");
	}

	}
}
