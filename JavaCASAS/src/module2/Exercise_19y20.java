package module2;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.stream.DoubleStream;

public class Exercise_19y20 {
	public static void wait(int ms){
        try
        {
            Thread.sleep(ms);
        }
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }

	
	public static void main(String[] args) {
		
		double numbers[];
				
		Scanner s = new Scanner(System.in);	
		double min = 0.0;
		numbers = new double[10];
	    int i=0;  
	    do{  
	    	
	        numbers[i] = (int)(Math.random()*100);  
	         
	        System.out.println("\n Los numeros ingresados son:"+ Arrays.toString(numbers));
	       
	        System.out.println("\n El valor mayor es: "+ (Arrays.stream(numbers).max().getAsDouble()));
	        
	        
	        if (i==0) {
	        	min = numbers[i];
	        }else{if (numbers[i]<min) {
	        	min = numbers[i];
	        }}
	        i++; 
	        
	        
	        System.out.println("\n El valor menor es: "+ (min));
	        //System.out.println("\n El valor menor es: "+ (Arrays.stream(numbers).min().getAsDouble()));
	        
	        System.out.println("\n La suma de los valores es: "+ (DoubleStream.of(numbers).sum()));
	        
	        System.out.println("\n El promedio de los valores es: "+ (DoubleStream.of(numbers).sum())/i);

	        wait(1000);
	        
	    
	    }while(i<10);  
		
		
		
		
	}

}
