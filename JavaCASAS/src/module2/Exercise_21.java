package module2;

import java.util.Scanner;

public class Exercise_21 {
	   
//Función para calcular porcentaje 
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		System.out.println("Ingrese la categoría (puede ser 'a', 'b', o 'c' en minúscula): ");
		char selected_category = s.next(). charAt(0);
		System.out.println("Ingrese la antigüedad en numero entero sin poner años (e.g. para ingresar 20 años solo ponga 20): ");
		int selected_antiquity = s.nextInt();
		System.out.println("Ingrese el sueldo bruto en $: ");
		double selected_income = s.nextDouble();
		int reward_antiquity = 30; 
		int reward_category = 0;
		int[] category_reward= {1000,2000,3000};
		int[] antiquity_reward= {5,5,5,5,5,10,10,10,10,10};
		int[] antiquity_correspondence= {1,2,3,4,5,6,7,8,9,10};
		char[] category_correspondence = {'a','b','c'}; 
		
		//Resolución de errores
		if (selected_antiquity==0 || selected_antiquity<0) {
			System.out.println("Usted ha trabajado menos de un año o no ha trabajado");
			System.exit(0);
		}
		if (selected_category != 'a' && selected_category != 'b' && selected_category != 'c') {
			System.out.println("Usted ha ingresado mal la categoría");
			System.exit(0);
		}
		
		
		//Determinar la posición de la antigüedad
		for (int i = 0; i<antiquity_reward.length; i++) {
			if (selected_antiquity==antiquity_correspondence[i]) {
				reward_antiquity = antiquity_reward[i];  
				//System.out.println("Por antigüedad se le sumara un: " +reward_antiquity+"%");
				
 			}
		}
		//Determinar la posición de la categoría
		for (int x = 0; x<category_reward.length; x++) {
			if (selected_category==category_correspondence[x]) {
				reward_category = category_reward[x];  
				//System.out.println("Por categoría se le sumaran : " +reward_category+"$");
 			}
		}
		//Calcular porcentaje
		double perc = (((double) reward_antiquity) / (100))*(selected_income + reward_category);
		double total = (selected_income + reward_category) + perc;
		
		//informarlo
		System.out.println("El ingreso neto será: " +total+ "$");
		System.out.println("Al sueldo neto se le agregarán: " +reward_category+ "$ por categoría");
		System.out.println("Un " +reward_antiquity+ "% del sueldo total (" +perc+ ") será de la antigüedad");
		
		
		
	}

}
