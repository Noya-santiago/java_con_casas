package module2;

//ingresar librería de input de numeros
import java.util.Scanner;


public class Exercise_3 {
	public static void main(String[] args) {
		System.out.println("Ingrese el mes del que quiere saber los días (sin punto final y con mayúscula al principio)");
		//Inicializar dos arrays de R= 1x12 indicando Días del mes y otro indicando nombres de los meses 
		
		String[] Months = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio","Julio","Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
		int[] Days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
		//Ingresar el string de mes
		Scanner s = new Scanner(System.in);
		
		//Ingresar variable de error en el ingreso
		int error = 0;
		
		String Selected_month = s.nextLine();
		
		//Comparar por cada valor si la entrada es igual a algun indice del string de meses
		//Si estos son iguales, indicar el valor del array al que corresponde y también su homologo 
		//En el string de meses, esto se logra mediante al contador del For 
		for (int i = 0; i<Months.length; i++ ) {
			if (Selected_month.equals(Months[i])) {
				System.out.println("El mes seleccionado es: "+Months[i]+ ", y tiene un total de: " +Days[i]+ " Días");
				
				error++; 
			}	
		}
		
		//Resolución de errores 
		if (error == 0) {
			System.out.println("Ha ingresado el mes mal, o un mes no perteneciente al calendario gregoriano");
		}
	
	}
}
