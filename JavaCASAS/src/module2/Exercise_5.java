package module2;
//Inicializar la librería de escaneo de numeros
import java.util.Scanner;

public class Exercise_5 {
	public static void main(String[] args) {
		/*
		Este programa utiliza exactamente la misma lógica 
		que el anterior, por lo tanto es basicamente 
		un copypaste xd
		*/
		
		System.out.println("Ingrese la Posición");
		//Inicializar dos arrays de R= 1x3 indicando la posición y sus dos homologos	
		String[] Position = {"Primero", "Segundo", "Tercero"};
		String[] Reward = {"oro", "plata", "bronce"};
		int[] Numeric_Position = {1, 2, 3};
		
		Scanner s = new Scanner(System.in);
	
		//Ingresar variable los que queden 4tos o mas
		int loosers = 0;
		
		int Entered_Position = s.nextInt();
		
		for (int i = 0; i<Position.length; i++ ) {
			if (Entered_Position == Numeric_Position[i]) {
				System.out.println("El "+Position[i]+ " obtiene la medalla de " +Reward[i]);			
				loosers++; 
			}	
		}
		
		if (Entered_Position <= 0 ) {
			loosers++;
			System.out.println("Ha ingresado su posición mal ");
		}
		//Perdedores y resolución de errores 
		if (loosers == 0) {
			System.out.println("Siga participando :(");
		}
		
		
	}
}
