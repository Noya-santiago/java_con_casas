package module2;

import java.util.Scanner;

public class Exercise_9 {
	public static void main(String[] args) {
		
		Scanner s = new Scanner (System.in);
		System.out.println("Ingrese el valor que jugará pepe:");
		int pepe = s.nextInt();
		
		System.out.println("Ingrese el valor que jugará pedro:");
		int pedro = s.nextInt();
		
		//Situaciones en donde pepe gana 
		if (pepe == 0 && pedro ==2) {
			System.out.println("Pepe es el ganador");
		}
		
		if (pepe == 1 && pedro ==0) {
			System.out.println("Pepe es el ganador");
		}
		
		if (pepe ==2 && pedro ==1 ) {
			System.out.println("Pepe es el ganador");
		}
		
		//Situaciones en donde Pedro gana 
		
		if (pedro==0 && pepe ==2) {
			System.out.println("Pedro es el ganador");
		}
		
		if (pedro ==1 && pepe == 0) {
			System.out.println("Pedro es el ganador");
		}
		
		if (pedro == 2 && pepe == 1 ) {
			System.out.println("Pedro es el ganador"); 
		}

		//situacion de empate
		
		if (pedro == pepe) {
			System.out.println("Pepe y pedro han empatado, deberán resolver sus diferencias a base de un duelo a muerte con cuchillos");
		}
	}
      
}
