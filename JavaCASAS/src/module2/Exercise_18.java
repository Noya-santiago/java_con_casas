package module2;

import java.util.Scanner;

public class Exercise_18 {

	public static void main(String[] args) {
	
		int trigger =1; 
		
		System.out.println("Ingrese la cantidad de tablas que quiere mostrar");
		
		Scanner s = new Scanner(System.in);
		int number = s.nextInt();
		int accum = 0;
		
		for(int i=1; i<=number; i++) {
			for(int x=0; x<10; x++) {
				accum += i; 
				System.out.println(accum);
				
			}
			accum =0; 
			
			if (i==number) {
				System.out.println("La tabla del: "+i+" ha terminado. Gracias por utilizar mi programa");
			}else {
				System.out.println("La tabla del: "+i+" ha terminado, ahora se imprimirá la tabla del "+(i+1));
			}
			
			
		}
		
		
	}
	
	
	
}
