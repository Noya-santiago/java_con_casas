package module2;

import java.util.Scanner;

public class Exercise_6 {
	public static void main(String[] args) {

		System.out.println("Ingrese el curso: ");
		Scanner s = new Scanner(System.in);
		int number = s.nextInt();
		/* la logica de este programa consiste en 
		 * preguntar 3 veces para determinar en donde 
		 * se encuentra el valor, al encontrar su posición 
		 * se informa la salida especificada, este ejercicio
		 * tambien se puede hacer con multiples arrays como los 
		 * anteriores, pero asumí que en este no estaba permitido.
		 */
		
		if(number>0 && number<7) {
			System.out.println("Primaria");
		}else {
				if(number >7 && number <13) {
					System.out.println("Secundaria");
				}else {
					if(number == 0) {
						System.out.println("Jardín");
					}
					else {
						System.out.println("Ha ingresado el numero mal");
					}
				}
			}
		}
		
}