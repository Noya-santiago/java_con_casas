
package objetosPractica2.view;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import objetosPractica2.modelo.Circle;
import objetosPractica2.modelo.Figure;
import objetosPractica2.modelo.Polygon;
import objetosPractica2.modelo.Rectangle;
import objetosPractica2.modelo.Square;
import objetosPractica2.modelo.Triangle;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import net.miginfocom.swing.MigLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class FigureView extends javax.swing.JFrame {

    private List<Figure> figuresList;
    private String arrayFigures[][];
    private Figure figToModDel;

    public FigureView() {
        initComponents();
        fillGrid(getInitCharge());
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {
        jTable1 = new javax.swing.JTable();


        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                jLabel2 = new javax.swing.JLabel();
                
                        jLabel2.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
                        jLabel2.setForeground(new java.awt.Color(102, 102, 102));
                        jLabel2.setText("Figuras");
                jBtnAdd = new javax.swing.JButton();
                
                        jBtnAdd.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
                        jBtnAdd.setText("Agregar");
                        jBtnAdd.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jBtnAddActionPerformed(evt);
                            }
                        });
                        jLabel1 = new javax.swing.JLabel();
                        
                                jLabel1.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
                                jLabel1.setForeground(new java.awt.Color(102, 102, 102));
                                jLabel1.setText("Noya ");
                jTextName = new javax.swing.JTextField();
                
                        jTextName.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
                        jTextName.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                actionPerformed(evt);
                            }
                        });
                        jLblName = new javax.swing.JLabel();
                        
                                jLblName.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
                                jLblName.setText("Nombre");
                        jBtnSquare = new javax.swing.JRadioButton();
                        
                                jBtnSquare.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
                                jBtnSquare.setSelected(true);
                                jBtnSquare.setText("Cuadrado");
                                jBtnSquare.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        jBtnSquareActionPerformed(evt);
                                    }
                                });
                jBtnCircle = new javax.swing.JRadioButton();
                
                        jBtnCircle.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
                        jBtnCircle.setText("Circulo");
                        jBtnCircle.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jBtnCircleActionPerformed(evt);
                            }
                        });
                        jBtnMod = new javax.swing.JButton();
                        
                                jBtnMod.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
                                jBtnMod.setText("Modificar");
                                jBtnMod.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        jBtnModActionPerformed(evt);
                                    }
                                });
                        jLblValue = new javax.swing.JLabel();
                        
                                jLblValue.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
                                jLblValue.setText("Lados");
                        jTextVal = new javax.swing.JTextField();
                        
                                jTextVal.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
                                jTextVal.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                                        actionPerformed(evt);
                                    }
                                });
                jBtnDel = new javax.swing.JButton();
                
                        jBtnDel.setFont(new java.awt.Font("Monospaced", 1, 18)); // NOI18N
                        jBtnDel.setText("Eliminar");
                        jBtnDel.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                jBtnDelActionPerformed(evt);
                            }
                        });
        
               // buttonGroup1 = new javax.swing.ButtonGroup();
              //  jFrame1 = new javax.swing.JFrame();
                jScrollPane1 = new javax.swing.JScrollPane();
                
                        jScrollPane1.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
                        
                                jTable1 = new javax.swing.JTable(){
                                    public boolean isCellEditable(int rowIndex, int colIndex){
                                        return false;
                                    }
                                };
                                jTable1.setAutoCreateRowSorter(true);
                                jTable1.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
                                jTable1.setModel(new javax.swing.table.DefaultTableModel(
                                    new Object [][] {

                                    },
                                    new String [] {

                                    }
                                ));
                                jTable1.setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
                                jTable1.setDragEnabled(true);
                                jTable1.setFocusable(false);
                                jTable1.setOpaque(false);
                                jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
                                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                                        jTable1MouseClicked(evt);
                                    }
                                });
                                jScrollPane1.setViewportView(jTable1);
                                        
                                        jText1v = new JTextField();
                                        jText1v.setVisible(false);
                                        jText1v.setColumns(10);
                                        
                                        jText2v = new JTextField();
                                        jText2v.setVisible(false);
                                        jText2v.setColumns(10);
                                        
                                        jLbl2V = new JLabel();
                                        jLbl2V.setVisible(false);
                                        jLbl2V.setText("Altura");
                                        jLbl2V.setFont(new Font("Monospaced", Font.BOLD, 18));
                                        
                                        jBtnTriangle2 = new JRadioButton("Triangle");
                                        jBtnTriangle2.addActionListener(new ActionListener() {
                                        	public void actionPerformed(ActionEvent arg0) {
                                        		
                                        		jBtnTriangle2.setSelected(true);
                                        		setTriangle();
                                        		
                                        	}
                                        });
                                        jBtnTriangle2.addKeyListener(new KeyAdapter() {
                                        	@Override
                                        	public void keyPressed(KeyEvent e) {
                                        		    	jBtnTriangle2.setSelected(true);
                                        		    	setTriangle();
                                        	}
                                        });
                                        jBtnTriangle2.addMouseListener(new MouseAdapter() {
                                        	@Override
                                        	public void mouseClicked(MouseEvent e) {
                                        	}
                                        });
                                        jBtnTriangle2.setFont(new Font("Monospaced", Font.BOLD, 36));
                                        
                                        jBtnRectangle = new JRadioButton("Rectangle");
                                        jBtnRectangle.addActionListener(new ActionListener() {
                                        	public void actionPerformed(ActionEvent arg0) {
                                        		setRectangle();
                                        	}
                                        });
                                        jBtnRectangle.setFont(new Font("Monospaced", Font.BOLD, 36));
                                        
                                        jBtnPolygon = new JRadioButton("Poligono");
                                        jBtnPolygon.addActionListener(new ActionListener() {
                                        	public void actionPerformed(ActionEvent arg0) {
                                        		setPolygon();
                                        	}
                                        });
                                        jBtnPolygon.setFont(new Font("Monospaced", Font.BOLD, 36));
                                        
                                        jLbl1v = new JLabel("Base");
                                        jLbl1v.setVisible(false);
                                        jLbl1v.setFont(new Font("Monospaced", Font.BOLD, 18));
                                        
                                        jTxtMaxSurf = new JTextField();
                                        jTxtMaxSurf.setEditable(false);
                                        jTxtMaxSurf.setEnabled(false);
                                        jTxtMaxSurf.setHorizontalAlignment(SwingConstants.CENTER);
                                        jTxtMaxSurf.setColumns(10);
                                        
                                        JLabel lblMaximaSuperficie = new JLabel("Maxima superficie");
                                        lblMaximaSuperficie.setFont(new Font("Monospaced", Font.BOLD, 12));
                                        GroupLayout groupLayout = new GroupLayout(getContentPane());
                                        groupLayout.setHorizontalGroup(
                                        	groupLayout.createParallelGroup(Alignment.LEADING)
                                        		.addGroup(groupLayout.createSequentialGroup()
                                        			.addGap(389)
                                        			.addComponent(jLabel2)
                                        			.addContainerGap(633, Short.MAX_VALUE))
                                        		.addGroup(groupLayout.createSequentialGroup()
                                        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        				.addGroup(groupLayout.createSequentialGroup()
                                        					.addContainerGap()
                                        					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
                                        						.addComponent(jBtnMod, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                                        						.addComponent(jBtnDel, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                                        						.addComponent(jBtnAdd, GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE))
                                        					.addGap(95))
                                        				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
                                        					.addGap(68)
                                        					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
                                        						.addComponent(lblMaximaSuperficie, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        						.addComponent(jTxtMaxSurf, Alignment.LEADING))
                                        					.addGap(149)))
                                        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        				.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
                                        					.addComponent(jTextName, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
                                        					.addComponent(jTextVal, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
                                        					.addGroup(groupLayout.createSequentialGroup()
                                        						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
                                        							.addComponent(jLbl1v, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        							.addComponent(jText1v))
                                        						.addPreferredGap(ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                                        						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
                                        							.addComponent(jLbl2V, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        							.addComponent(jText2v, GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE))))
                                        				.addComponent(jLblValue)
                                        				.addComponent(jLblName))
                                        			.addGap(126)
                                        			.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
                                        				.addComponent(jBtnCircle)
                                        				.addComponent(jBtnRectangle)
                                        				.addComponent(jBtnPolygon)
                                        				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
                                        					.addComponent(jBtnSquare, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        					.addComponent(jBtnTriangle2, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        			.addGap(209))
                                        		.addGroup(groupLayout.createSequentialGroup()
                                        			.addGap(421)
                                        			.addComponent(jLabel1)
                                        			.addContainerGap(647, Short.MAX_VALUE))
                                        		.addGroup(groupLayout.createSequentialGroup()
                                        			.addGap(6)
                                        			.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 964, GroupLayout.PREFERRED_SIZE)
                                        			.addContainerGap(213, Short.MAX_VALUE))
                                        );
                                        groupLayout.setVerticalGroup(
                                        	groupLayout.createParallelGroup(Alignment.TRAILING)
                                        		.addGroup(groupLayout.createSequentialGroup()
                                        			.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        				.addGroup(groupLayout.createSequentialGroup()
                                        					.addGap(130)
                                        					.addComponent(jBtnAdd)
                                        					.addPreferredGap(ComponentPlacement.RELATED)
                                        					.addComponent(jBtnMod)
                                        					.addPreferredGap(ComponentPlacement.RELATED)
                                        					.addComponent(jBtnDel))
                                        				.addGroup(groupLayout.createSequentialGroup()
                                        					.addComponent(jLabel2)
                                        					.addPreferredGap(ComponentPlacement.RELATED)
                                        					.addComponent(jLabel1)
                                        					.addGap(14)
                                        					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        						.addGroup(groupLayout.createSequentialGroup()
                                        							.addComponent(jLblName)
                                        							.addPreferredGap(ComponentPlacement.RELATED)
                                        							.addComponent(jTextName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        							.addPreferredGap(ComponentPlacement.RELATED)
                                        							.addComponent(jLblValue)
                                        							.addPreferredGap(ComponentPlacement.RELATED)
                                        							.addComponent(jTextVal, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        							.addPreferredGap(ComponentPlacement.RELATED)
                                        							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
                                        								.addComponent(jLbl2V, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                                        								.addComponent(jLbl1v)
                                        								.addComponent(lblMaximaSuperficie))
                                        							.addPreferredGap(ComponentPlacement.RELATED)
                                        							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
                                        								.addComponent(jText2v, GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                                        								.addComponent(jText1v, GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                                        								.addComponent(jTxtMaxSurf)))
                                        						.addGroup(groupLayout.createSequentialGroup()
                                        							.addComponent(jBtnSquare)
                                        							.addPreferredGap(ComponentPlacement.UNRELATED)
                                        							.addComponent(jBtnCircle)
                                        							.addPreferredGap(ComponentPlacement.RELATED)
                                        							.addComponent(jBtnTriangle2, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                                        							.addGap(4)
                                        							.addComponent(jBtnRectangle, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                                        							.addPreferredGap(ComponentPlacement.UNRELATED)
                                        							.addComponent(jBtnPolygon)))))
                                        			.addGap(18)
                                        			.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 212, GroupLayout.PREFERRED_SIZE)
                                        			.addGap(318))
                                        );
                                        getContentPane().setLayout(groupLayout);

        pack();
    }// </editor-fold>                        

    private void jBtnCircleActionPerformed(java.awt.event.ActionEvent evt) {                                           
        jBtnCircle.setSelected(true);    
        setCirc();     
    }                                          

    private void jBtnSquareActionPerformed(java.awt.event.ActionEvent evt) {                                           
        jBtnSquare.setSelected(true);
        setSquare();
    }                                          

    private void jBtnAddActionPerformed(java.awt.event.ActionEvent evt) {                                        
        if(jBtnSquare.isSelected()){
        	createSquare();
        }else if (jBtnCircle.isSelected()){
        	createCircle();
        }else if (jBtnTriangle2.isSelected()) {
        	createTriangle();
        }else if (jBtnRectangle.isSelected()) {
        	createRectangle();
        }else if (jBtnPolygon.isSelected()) {
        	createPolygon();
        }
        fillGrid(figuresList);
        cleanFields();
    }                                       

    private void jBtnModActionPerformed(java.awt.event.ActionEvent evt) {                                        
        
        if(figToModDel==null){
            JOptionPane.showMessageDialog(null, "Seleccione una figura");
        }else{
            figToModDel.setName(jTextName.getText());
            
            if(figToModDel instanceof Circle){
            	setCirc();
                ((Circle)figToModDel).setRadius(Integer.parseInt(jTextVal.getText()));
            }else if (figToModDel instanceof Square){
            	setSquare();
                ((Square)figToModDel).setSide(Integer.parseInt(jTextVal.getText()));
            }else if (figToModDel instanceof Triangle) {
            	setTriangle();
            	((Triangle)figToModDel).setBase(Integer.parseInt(jTextVal.getText()));
            	((Triangle)figToModDel).setHeight(Integer.parseInt(jText1v.getText()));
            }else if (figToModDel instanceof Rectangle) {
            	setRectangle();
            	((Rectangle)figToModDel).setBase(Integer.parseInt(jTextVal.getText()));
            	((Rectangle)figToModDel).setHeight(Integer.parseInt(jText1v.getText()));
            	
            }else if (figToModDel instanceof Polygon) {
            	setPolygon();
            	((Polygon)figToModDel).setSides(Float.parseFloat(jTextVal.getText()));
            	((Polygon)figToModDel).setSideAmmount(Integer.parseInt(jText1v.getText()));
            	((Polygon)figToModDel).setApothem(Double.parseDouble(jText2v.getText()));
            }
            
            figToModDel = null; 
            fillGrid(figuresList);
            cleanFields();          
        }   
        
    }                                       

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {                                     
        
        System.out.println("Se ha presionado la fila = "+ jTable1.getSelectedRow());
        figToModDel = figuresList.get(jTable1.getSelectedRow());        
        
        if(figToModDel instanceof Circle){
            assignVals((Circle)figToModDel);
        }else if (figToModDel instanceof Square){
            assignVals((Square)figToModDel);
        }else if (figToModDel instanceof Triangle) {
        	assignVals((Triangle)figToModDel);
        }else if (figToModDel instanceof Rectangle) {
        	assignVals((Rectangle)figToModDel);
        }else if (figToModDel instanceof Polygon) {
        	assignVals((Polygon)figToModDel);
        }
       
        if(evt.getClickCount()==2){
            JFrame frame = new JFrame("ex");
            JOptionPane.showMessageDialog(frame, figToModDel.toString());
        }
    }                                    

    
    
    
    
    private void jBtnDelActionPerformed(java.awt.event.ActionEvent evt) {                                        
        
        if(figToModDel==null){
            JOptionPane.showMessageDialog(null, "Select a figure to be deleted");
        }else{
            figuresList.remove(figToModDel);
            figToModDel = null;
        }
        fillGrid(figuresList);
        cleanFields();
    }                                       

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FigureView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FigureView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FigureView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FigureView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FigureView().setVisible(true);
            }
        });
    }
        
    private void assignVals(Circle circ){
        setCirc();    
    	jTextName.setText(circ.getName());
        jTextVal.setText(Float.toString(circ.getRadius()));
    }
    private void assignVals(Square sq){
        setSquare();    
    	jTextName.setText(sq.getName());
        jTextVal.setText(Float.toString(sq.getSide()));
    }
   
    private void assignVals(Rectangle rect) {
    	setRectangle();
    	jTextName.setText(rect.getName());
    	jTextVal.setText(Float.toString(rect.getBase()));
    	jText1v.setText(Float.toString(rect.getHeight()));
    }
    
   private void assignVals(Triangle tri) {
	   setTriangle();
	   jTextName.setText(tri.getName());
	   jTextVal.setText(Float.toString(tri.getBase()));
	   jText1v.setText(Float.toString(tri.getHeight()));
   } 
   
   private void assignVals(Polygon pol) {
	   setPolygon();
	   jTextName.setText(pol.getName());
	  
	   jTextVal.setText(Float.toString(pol.getSides()));
	   jText1v.setText(Integer.toString(pol.getSideAmmount()));
	   jText2v.setText(Double.toString(pol.getApothem()));
   }
    private void setCirc(){
    	select();
    	jBtnCircle.setSelected(true);
        jLblValue.setText("Radio");
    }
   
    private void setSquare(){
    	select();
    	jBtnSquare.setSelected(true);
    	jLblValue.setText("Lados");
    }
    
    private void setTriangle() {
    	select();
    	jBtnTriangle2.setSelected(true);
        jLblValue.setText("Base");   
    	jText1v.setVisible(true);
    	jLbl1v.setVisible(true);
    	jLbl1v.setText("Altura");
    }
    
    private void setRectangle() {
    	
    	select();
    	jBtnRectangle.setSelected(true);
    	jText1v.setVisible(true);
    	jLbl1v.setVisible(true);
    	jLblValue.setText("Base");
        jLbl1v.setText("Altura");
        
    }
    private void createPolygon() {
    	String polName = jTextName.getText();
    	float sides = Float.parseFloat(jTextVal.getText());
        int sideAmmount = Integer.parseInt(jText1v.getText());
        double apothem = Double.parseDouble(jText2v.getText());
        figuresList.add(new Polygon(polName, sides, sideAmmount, apothem));
    }
    
    
    private void createCircle() {
    	String circName = jTextName.getText();
    	float radiusVal = Float.parseFloat(jTextVal.getText());
    	figuresList.add(new Circle(circName, radiusVal));
    }
    private void createSquare() {
    	String squareName = jTextName.getText();
    	float sideVal = Float.parseFloat(jTextVal.getText());
    	figuresList.add(new Square(squareName,sideVal));
    	
    } 
    private void createRectangle() {
    	String rectname = jTextName.getText();
    	float baseVal = Float.parseFloat(jTextVal.getText());
    	float heightVal = Float.parseFloat(jText1v.getText());
    	figuresList.add(new Rectangle(rectname,baseVal,heightVal));
    }
    private void createTriangle() {
    	String triName = jTextName.getText();
    	float baseVal = Float.parseFloat(jTextVal.getText());
    	float heightVal = Float.parseFloat(jText1v.getText());
    	figuresList.add(new Triangle(triName, baseVal, heightVal));
    }
    private void select() {
        jBtnCircle.setSelected(false);
        jBtnSquare.setSelected(false);
        jBtnTriangle2.setSelected(false);
        jBtnRectangle.setSelected(false);
        jBtnPolygon.setSelected(false);
    	jText1v.setVisible(false);
    	jText2v.setVisible(false);
    	jLbl2V.setVisible(false);
    	jLbl1v.setVisible(false);
    }
    
    private void setPolygon() {
    	select();
    	jLblValue.setText("Lado");
    	jLbl1v.setText("Nº Lados");
    	jLbl2V.setText("Apotema");
    	jBtnPolygon.setSelected(true);
    	jText1v.setVisible(true);
    	jLbl1v.setVisible(true);
    	jText2v.setVisible(true);
    	jLbl2V.setVisible(true);
    }
      
    private List<Figure> getInitCharge(){
        figuresList = new ArrayList<Figure>();
        figuresList.add(new Square("MartinElCuadrado", 20));
        figuresList.add(new Circle("PedroElCirculo", 10));
        return figuresList;
    }

  
    private void cleanFields(){
        jTextVal.setText("");
        jTextName.setText("");  
        jText1v.setText("");
        jText2v.setText("");
    }
  
    
    private void fillGrid(List<Figure> pFigureList){
        int row = 0;
        arrayFigures = new String[pFigureList.size()][4];
        for(Figure figure: pFigureList){        
            for(int col=0;col<4;col++){
                switch(col){
                    case 0:
                        arrayFigures[row][col] = figure.getName();
                        break;
                    case 1:
                        String temp;
                        temp = figure.getClass().getSimpleName();  
                        //temp = (figure.getClass().getSimpleName().equals("Circle"))? "Circulo" : "Cuadrado";
                        arrayFigures[row][col] = temp;
                        break;
                    case 2:
                        arrayFigures[row][col] = Float.toString(Math.round(figure.calcPerimeter() * 10) / 10);
                        break;
                    case 3: 
                        arrayFigures[row][col] = Float.toString(Math.round(figure.calcSurface() * 10) / 10);
                        break;
                }
            }
        row++;
        }
        jTxtMaxSurf.setText(String.valueOf(Figure.getMaxSurface()));
        //Assign to grid
        jTable1.setModel(new DefaultTableModel(arrayFigures,new String[]{"Nombre","Figura","Perimetro","Superficie"}));
        
        System.out.println(Figure.getMaxSurface());
    }
    
    // Variables declaration - do not modify                     
   // private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jBtnAdd;
    private javax.swing.JRadioButton jBtnCircle;
    private javax.swing.JButton jBtnDel;
    private javax.swing.JButton jBtnMod;
    private javax.swing.JRadioButton jBtnSquare;
   // private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLblName;
    private javax.swing.JLabel jLblValue;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextName;
    private javax.swing.JTextField jTextVal;
    private JTextField jText1v;
    private JTextField jText2v;
    private JLabel jLbl2V;
    private JRadioButton jBtnTriangle2;
    private JRadioButton jBtnRectangle;
    private JRadioButton jBtnPolygon;
    private JLabel jLbl1v;
    private JTextField jTxtMaxSurf;
}