package objetosPractica2.modelo;

public class Rectangle extends Figure {
	
	private float base; 
	private float height; 
	
	public Rectangle() {}

	
	public Rectangle (String pName) {
		super(pName);
	}
	
	public Rectangle (String pName, float pBase, float pHeight) {
		
		this(pName);
		this.base = pBase;
		this.height = pHeight; 
		Figure.maxSurf = Math.max(Figure.maxSurf,calcSurface());
		
	}
	
	
	
	public float getBase() {return base;}
	public void setBase(float pBase) {this.base=pBase;}
	
	public float getHeight() {return height;}
	public void setHeight(float pHeight) {this.height=pHeight;}
	
	
	
	
	
	@Override
	public float calcPerimeter() {
		return 2*(base+height);
	}
	
	@Override
	public float calcSurface() {
		return base*height;
	}

	@Override
	public String getVals() {
		StringBuffer sb = new StringBuffer("B= ");
		sb.append(base);
		sb.append("-H= ");
		sb.append(height);
		return sb.toString();
	}
	

	@Override
	public int hashCode() {
		final int prime = 31; 
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(height);
		result = prime * result + Float.floatToIntBits(base);
		return result;
	}	
		
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Rectangle)) {
			return false;
		}
		Rectangle other = (Rectangle) obj;
		if (Float.floatToIntBits(height) != Float.floatToIntBits(other.height)) {
			return false;
		}
		if (Float.floatToIntBits(base) != Float.floatToIntBits(other.base)) {
			return false;
		}
		return true;

		
	}
		
		
		
	
	
	


}
