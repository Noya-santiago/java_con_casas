package objetosPractica2.modelo;

abstract public class Figure implements Model {
	
	//Ejercicio 1 
	//Attributes	
	protected static float maxSurf;
	private String name;
	
	//Metodos
	
	abstract public float calcPerimeter();
	abstract public float calcSurface();
	
	//Ejercicio 2
	//Constructors
	
	public Figure () {}
	public Figure (String fName) {
		super();
		this.name = fName;
	}
	
	//Ejercicio 3 
	//Setters and Getters
	
	public String getName() {					//Getter
		return name;
	}
	
	public void setName(String fName) {			//Setter
		this.name = fName;
	}
	
	//Ejercicio 4 
	//Hash, Equals, toString
	
	public String toString() {					//toString
		StringBuffer sb = new StringBuffer("\n\nnombre=");
		sb.append(name);
		return sb.toString();
	}
	
	public int hashCode() {						//HashCode
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name== null) ? 0 : name.hashCode());
		return result;
	
		
		//return name.hashCode();
	}
	
	public boolean equals(Object obj) {			//Equals
		
		//Codigo bien hecho
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Figure)) {
			return false;
		}
		//downcast
		Figure other = (Figure) obj;
		if (name== null) {
			if (other.getName() != null) {
				return false;
			}
		} else if (!name.equals(other.getName())) {
			return false;
		}
		return true;
		
		//Código mío xd 
		/*boolean bln = false;
		if (obj instanceof Figure) {
			Figure fig = (Figure)obj;
			
			bln = fig.getName() != null && fig.getName().equals(name);
			
		}
		return bln;*/
	}

	public static float getMaxSurface() {
		
		return maxSurf;
	
	}

	
	
	public abstract String getVals();


}
