package objetosPractica2.modelo;

public class Polygon extends Figure {

	
	
	private float sides; 
	private int sideAmmount; 
	private double apothem;
	
	public Polygon() {}
	public Polygon(String pName) {
		super(pName);
		
	}
	public Polygon(String pName,float pSides, int pSideAmmount, double pApothem) {
		
		this(pName);
		this.sides= pSides;
		this.sideAmmount = pSideAmmount; 
		this.apothem = pApothem;
		Figure.maxSurf = Math.max(Figure.maxSurf,calcSurface());
	}

	//Getters
	
	public float getSides() {return sides;}
	public int getSideAmmount() {return sideAmmount;}
	public double getApothem() {return apothem;}

	@Override
	public String getVals() {
		StringBuffer sb = new StringBuffer("sA= ");
		sb.append(sideAmmount);
		sb.append("sV= ");
		sb.append(sides);
		sb.append("A= ");
		return sb.toString();
	}

	//Setters
	public void setSides(float pSides) {this.sides = pSides;}
	
	public void setSideAmmount(int pSideAmmount) {this.sideAmmount = pSideAmmount;}
	
	public void setApothem(double pApothem) {this.apothem=pApothem;}
	
	
	
	@Override
	public float calcPerimeter() {

		return sides * sideAmmount; 
	}

	@Override
	public float calcSurface() {
	
		float surface = (float) ((sides*sideAmmount*apothem)/2);
	
		return surface;
		
	}
	//Equals, hashcode y ToString
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Polygon)) {
			return false;
		}
		Polygon other = (Polygon) obj;
		if (Float.floatToIntBits(sides) != Float.floatToIntBits(other.sides)) {
			return false;
		}
		if (Integer.toBinaryString(sideAmmount) != Integer.toBinaryString(other.sideAmmount)) {
			return false;
		}
		if (Float.floatToIntBits((float) apothem) != Float.floatToIntBits((float) other.apothem)) {
			return false; 
		}
		return true;
	}	
	
	@Override
	public String toString() {
		
		return super.toString() + "\n sA = " + sideAmmount + "\n sV = " + sides + "\n A= " + apothem; 
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31; 
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(sides);
		result = prime * result + sideAmmount; 
		result = (int) (prime * result + Double.doubleToLongBits(apothem));
		return result; 	
	}
	
	

}
