package objetosPractica2.modelo;

public class Square extends Figure{
	
	//Ejercicio 1 
	//Attributes
	
	float side;

	
	//Inherited Methods 
	public float calcPerimeter() {
		return (float) (side *4) ;
	}

	public float calcSurface() {
		return (float)(side*side);
	}
	
	//Ejercicio 2 
	//constructors 
	
	public Square () {} 							//No parameters
	
	public Square(String name) {					//1 Parameter: {String}
		super(name);
	}
	
	public Square(String name, float fSide) {		//2 Parameters: {String, float}
		super(name);
		this.side = fSide;	
		Figure.maxSurf= Math.max(Figure.maxSurf, calcSurface());
	}
	
	//Ejercicio 3 
	//Getters and Setters
	
	public float getSide() {						//Getters
		return side;
	}
	
	public void setSide(int fSide) {				//Setters
		this.side = fSide;
	}
	
	//Ejercicio 4 Hash Code, equals and toString	
	public boolean equals(Object obj) {				//equals
		boolean bln = false; 
		if (obj instanceof Figure) {
			Square cuadr = (Square) obj;
			bln = super.equals(obj) && cuadr.getSide() == side;
		}
		return bln;
	}
	
	//hash
	@Override
	public int hashCode() {							//hashCode
		return super.hashCode() + (int)(side);
	}
	
	//ToString
	public String toString() {						//toString
		StringBuffer sb = new StringBuffer(super.toString());
		sb.append("\nlado= ");
		sb.append(side);
		return sb.toString();
	}

	@Override
	public String getVals() {
		// TODO Auto-generated method stub
		return null;
	}

}