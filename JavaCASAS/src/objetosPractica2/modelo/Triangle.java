package objetosPractica2.modelo;

public class Triangle extends Figure {
	private float base; 
	private float height; 
	
	public Triangle() {}

	public Triangle(String pName) {super(pName);}
	

	public Triangle(String pName, float pBase, float pHeight) {
		this(pName);
		this.base= pBase;
		this.height=pHeight;
		Figure.maxSurf = Math.max(Figure.maxSurf, calcSurface());
		
	}
	
	public float getBase() {return base;}
	public void setBase(float pBase) {this.base=pBase;}
	
	
	public float getHeight() {return height;}
	public void setHeight(float pHeight) {this.height = pHeight;}
	
	@Override
	public String toString() {
		
		return super.toString() + "\nBase= " + base + "\nAltura= " + height; 
		
	}
	
	
	@Override
	public float calcPerimeter() {
	
	
		float hip = (float)Math.sqrt(height*height + base*base);
		return base+height+hip;
	
	
	}

	@Override
	public float calcSurface() {
	
		return base*height/2;
	
	}

	@Override
	public String getVals() {
	
		StringBuffer sb = new StringBuffer("B= ");
		sb.append(base);
		sb.append("-A= ");
		sb.append(height);
		return sb.toString();
	}

	@Override
	public int hashCode() {
		
		final int prime =31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(height);
		result = prime * result + Float.floatToIntBits(base);
		return result; 
	}	
		
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Triangle)) {
			return false;
		}
		Triangle other = (Triangle) obj;
		if (Float.floatToIntBits(height) != Float.floatToIntBits(other.height)) {
			return false;
		}
		if (Float.floatToIntBits(base) != Float.floatToIntBits(other.base)) {
			return false;
		}
		return true;

	}
		
	}
