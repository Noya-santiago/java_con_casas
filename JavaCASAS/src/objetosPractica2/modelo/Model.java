package objetosPractica2.modelo;

public interface Model {
	
	public boolean equals(Object obj);
	public int hashCode();
	public String toString();
	
}


