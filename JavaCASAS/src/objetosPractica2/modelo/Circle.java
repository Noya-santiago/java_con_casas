package objetosPractica2.modelo;

public class Circle extends Figure{
	
	
	//Ejercicio 1 
	//Attributes
	
	float radius;

	//Inherited Methods
	public float calcPerimeter() {
		return (float) (radius *6.283184) ;
	}

	public float calcSurface() {
		return (float)(radius*radius*3.141592);
	}
	
	//Ejercicio 2 
	//constructors
	
	public Circle () {}								//No parameters
	
	public Circle(String name) {					//1 Parameter: {string}
		super(name);
	}
	
	public Circle(String name, float fRadius) {	//2 Parameters: {String, float}
		super(name);
		this.radius = fRadius;	
		Figure.maxSurf= Math.max(Figure.maxSurf, calcSurface());
	}
	
	//Getters
	public float getRadius() {
		return radius;
	}
	//Setters
	public void setRadius(int fRadius) {
		this.radius = fRadius;
	}
	
	//equals
	public boolean equals(Object obj) {
		boolean bln = false; 
		if (obj instanceof Figure) {
			Circle circ = (Circle) obj;
			bln = super.equals(obj) && circ.getRadius() == radius;
		}
		return bln;
	}
	
	//hash
	@Override
	public int hashCode() {
		return super.hashCode() + (int)(radius);
	}
	
	//ToString
	public String toString() {
		StringBuffer sb = new StringBuffer(super.toString());
		sb.append("\nradio= ");
		sb.append(radius);
		return sb.toString();
	}

	@Override
	public String getVals() {
		// TODO Auto-generated method stub
		return null;
	}

		

	
	
	
	
}
