package objetosPractica2.modelo.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import objetosPractica2.modelo.Figure;
import objetosPractica2.modelo.Polygon;
import objetosPractica2.modelo.Rectangle;
import objetosPractica2.modelo.Circle;
import objetosPractica2.modelo.Square;
import objetosPractica2.modelo.Triangle;

@SuppressWarnings("deprecation")
public class Testing {

	List<Figure> figureList = new ArrayList<Figure>();
	Set<Figure> figureSet = new HashSet<Figure>();

	Square square; 
	Circle circle; 
	Rectangle rectangle;
	Triangle triangle; 
	Polygon polygon; 
	
	
	/* ____                _                   _
	  / ___|___  _ __  ___| |_ _ __ _   _  ___| |_ ___  _ __ ___
	 | |   / _ \| '_ \/ __| __| '__| | | |/ __| __/ _ \| '__/ __|
	 | |__| (_) | | | \__ \ |_| |  | |_| | (__| || (_) | |  \__ \
	  \____\___/|_| |_|___/\__|_|   \__,_|\___|\__\___/|_|  |___/	 
	 */
	@Before
	public void setUp() throws Exception {
		
		figureList.add(new Circle("Circ1", 10));
		figureList.add(new Square("Squar1", 5));
		figureList.add(new Triangle("Trian1", 2, 1));
		figureList.add(new Rectangle("Rect1", 2, 1));
		figureList.add(new Polygon("Pol1",5,5, 43.0119));
		
		figureSet.add(new Circle("Circ1", 10));
		figureSet.add(new Square("Squar1", 5));
		figureSet.add(new Triangle("Trian1", 2, 1));
		figureSet.add(new Rectangle("Rect1", 2, 1));
		figureSet.add(new Polygon("Pol1",5,5, 43.0119));
		
	}
	@After
	public void tearDown() throws Exception {
		figureList = null;
		figureSet = null; 
		
		square = null; 
		circle = null; 
		rectangle = null; 
		triangle = null; 
		polygon = null; 
	}
/*
	  ____            __
	 |  _ \ ___ _ __ / _| ___  _ __ _ __ ___   __ _ _ __   ___ ___
	 | |_) / _ \ '__| |_ / _ \| '__| '_ ` _ \ / _` | '_ \ / __/ _ \
	 |  __/  __/ |  |  _| (_) | |  | | | | | | (_| | | | | (_|  __/
	 |_|   \___|_|  |_|  \___/|_|  |_| |_| |_|\__,_|_| |_|\___\___|
*/
	@Test
	public void testMaxSurf() {
		
		assertEquals(1256.6368, Figure.getMaxSurface(),10);
		
	}
	
	
	
	
	
	@Test
	public void testAddList() {
		
		figureList.add(new Circle("Circ2", 20));
		
		assertEquals(3, figureList.size());
		
		System.out.println("figureList="+ figureList);
	}

	@Test
	public void testAddListContains() {
		assertTrue(figureList.contains(new Circle("Circ1", 10)));
	}
	
	@Test
	public void testAddSet(){
		
		figureSet.add(new Circle("Circ1", 10));
		assertEquals(2, figureList.size());
		System.out.println("personasSet=" + figureList);		
	}
	
	
	/*
	 _____		            _
	| ____|__ _ _   _  __ _| |___
	|  _| / _` | | | |/ _` | / __|
	| |__| (_| | |_| | (_| | \__ \
	|_____\__, |\__,_|\__,_|_|___/
         	 |_|	
	*/
	@Test
	public void testCircleEqualsTrue(){
		Circle proof1 = new Circle("Federico", 1);
		Circle proof2 = new Circle("Federico", 1);
		
		assertTrue(proof1.equals(proof2));
		
	}
	
	@Test
	public void testCircleEqualsFalse(){
		Circle proof1 = new Circle("Federico", 2);
		Circle proof2 = new Circle("Federico", 1);
		
		assertFalse(proof1.equals(proof2));
		
	}
	
	@Test
	public void testSquareEqualsTrue(){
		Square proof1 = new Square("Federico", 1);
		Square proof2 = new Square("Federico", 1);
		
		assertTrue(proof1.equals(proof2));
		
	}
	
	@Test
	public void testSquareEqualsFalse(){
		Square proof1 = new Square("Federico", 2);
		Square proof2 = new Square("Federico", 1);
		
		assertFalse(proof1.equals(proof2));
		
	}
	
	
	
}

