package objetosPractica2.controller;

import objetosPractica2.modelo.Figure;
import objetosPractica2.modelo.exception.FigureException;

public interface Controller {

	public void addHandler(Figure fig) throws FigureException; 
	public void leereHandler(Figure fig)throws FigureException;
	public void modifyHandler(Figure fig)throws FigureException;
	public void removeHandler(Figure fig)throws FigureException;
}
