package objetosPractica2.controller;

import objetosPractica2.controller.composite.ValidatorComposite;
import objetosPractica2.modelo.Figure;
import objetosPractica2.modelo.exception.FigureException;

public class FiguraController implements Controller{

	@Override
	public void addHandler(Figure fig)throws FigureException {
		// TODO Auto-generated method stub
		String strErr= ValidatorComposite.getErrors(fig);
		
		if(!strErr.isEmpty()) {
			throw new FigureException(strErr);
		}
	}

	@Override
	public void leereHandler(Figure fig) throws FigureException{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyHandler(Figure fig) throws FigureException{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeHandler(Figure fig) throws FigureException{
		// TODO Auto-generated method stub
		
	}

	public FiguraController() {};
	

}
