package objetosPractica2.controller.composite.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import objetosPractica2.controller.composite.ValidatorComposite;
import objetosPractica2.modelo.Circle;
import objetosPractica2.modelo.Figure;
import objetosPractica2.modelo.Rectangle;
import objetosPractica2.modelo.Square;

public class ValidatorCompositeTest {
	Circle figTest; 
	Circle figTest2;
	Square figTest3;
	Rectangle figTest4; 
	@Before
	public void setUp() throws Exception{
		figTest = new Circle("Cir  Double space", -10);
		figTest2 = new Circle("", -69420);
		figTest3 = new Square("  Pablo  ", -69420);
		figTest4 = new Rectangle("  pablo ", -999, -3);
	}
	
	@After
	public void tearDown() throws Exception{
		figTest=null;
	}

	@Test
	public void testGetErroresNombreDobleEspacioyRadioNegativo() {
		assertEquals("El nombre es invalido\nradius must be greater than zero.\n", ValidatorComposite.getErrors(figTest));
	
	}
	
	@Test
	public void testGetErroresNombreDobleEspacio() {
		figTest.setRadius(20);
		assertEquals("El nombre es invalido\n", ValidatorComposite.getErrors(figTest));
	}
	@Test
	public void testGetErroresRadioNegativo() {
		figTest.setName("circulo");
		assertEquals("radius must be greater than zero.\n", ValidatorComposite.getErrors(figTest));
	}
	
	@Test
	public void testEmptyName() {
		figTest2.setName("");
		figTest2.setRadius(69420);
		assertEquals("empty name\n", ValidatorComposite.getErrors(figTest2));
		
	}
	
	@Test
	public void testEmptyNameAndNegRad() {
		figTest2.setName("");
		figTest2.setRadius(-69420);
		assertEquals("empty name\nradius must be greater than zero.\n", ValidatorComposite.getErrors(figTest2));
		
	}

	@Test
	public void testNegSideSquare() {
		figTest3.setName("ValidName");
		assertEquals("Side must be > 0\n", ValidatorComposite.getErrors(figTest3));
	}

	@Test
	public void testNegSideSquareAndInvalidName() {
		assertEquals("El nombre es invalido\nSide must be > 0\n", ValidatorComposite.getErrors(figTest3));
	}

	@Test
	public void testEmptyNameAndNegSide() {
		figTest3.setName("");
		assertEquals("empty name\nSide must be > 0\n", ValidatorComposite.getErrors(figTest3));
	}

	@Test
	public void testBaseNegRec() {
		figTest4.setName("ValidName");
		figTest4.setHeight(3);
		assertEquals("base must be > 0\n", ValidatorComposite.getErrors(figTest4));
	}
	
	@Test
	public void testHeightNegRec() {
		figTest4.setName("ValidName");
		figTest4.setBase(3);
		assertEquals("height must be > 0\n", ValidatorComposite.getErrors(figTest4));
	}
	
	@Test
	public void testHeightBaseNeg() {
		figTest4.setName("ValidName");
		assertEquals("base must be > 0\nheight must be > 0\n", ValidatorComposite.getErrors(figTest4));
	}
	@Test
	public void testHeightNegAndInvalidName() {
		figTest4.setBase(2);
		assertEquals("El nombre es invalido\nheight must be > 0\n", ValidatorComposite.getErrors(figTest4));
	}	
	@Test
	public void testBaseNegAndInvalidName() {
		figTest4.setHeight(2);
		assertEquals("El nombre es invalido\nbase must be > 0\n", ValidatorComposite.getErrors(figTest4));
	}	

	@Test
	public void nameWrong() {
		figTest4.setBase(3);
		figTest4.setHeight(54);
		assertEquals("El nombre es invalido\n", ValidatorComposite.getErrors(figTest4));
	}
	@Test
	public void everythingWrong() {
		assertEquals("El nombre es invalido\nbase must be > 0\nheight must be > 0\n", ValidatorComposite.getErrors(figTest4));
	}
	@Test
	public void emptyName() {
		figTest4.setName("");
		figTest4.setBase(2);
		figTest4.setHeight(2);
		assertEquals("empty name\n", ValidatorComposite.getErrors(figTest4));
	}

	@Test
	public void emptyNameAndBaseNeg() {
		figTest4.setName("");
		figTest4.setHeight(2);
		assertEquals("empty name\nbase must be > 0\n", ValidatorComposite.getErrors(figTest4));
	}
	@Test
	public void emptyNameAndBaseHeight() {
		figTest4.setName("");
		figTest4.setBase(2);
		assertEquals("empty name\nheight must be > 0\n", ValidatorComposite.getErrors(figTest4));

	}
	@Test
	public void emptyNameCirc() {
		figTest2.setRadius(2);
		assertEquals("empty name\n", ValidatorComposite.getErrors(figTest2));
	}
	@Test
	public void emptyNameSqu() {
		figTest3.setName("");
		figTest3.setSide(2);
		assertEquals("empty name\n", ValidatorComposite.getErrors(figTest3));
	}
	@Test
	public void invalidNameRectangle() {
		figTest4.setName("  i  n  v  a  l  i  d  N  a  m  e");
		figTest4.setHeight(2);
		figTest4.setBase(3);
		assertEquals("El nombre es invalido\n", ValidatorComposite.getErrors(figTest4));
	}
	@Test
	public void invalidNameSquare() {
		figTest3.setName("  i  n  v  a  l  i  d  N  a  m  e");
		figTest3.setSide(2);
		assertEquals("El nombre es invalido\n", ValidatorComposite.getErrors(figTest3));
	}
	@Test
	public void emptyNameAndNegRad() {
		figTest2.setRadius(-3);
		figTest2.setName("");
		assertEquals("empty name\nradius must be greater than zero.\n", ValidatorComposite.getErrors(figTest2));
	}
	@Test
	public void emptyNameAndNegSide() {
		figTest3.setSide(-3);
		figTest3.setName("");
		assertEquals("empty name\nSide must be > 0\n", ValidatorComposite.getErrors(figTest3));
	}
	@Test
	public void everythingWrongWithEmptyName() {
		figTest4.setName("");
		assertEquals("empty name\nbase must be > 0\nheight must be > 0\n", ValidatorComposite.getErrors(figTest4));
	}
}
