
package objetosPractica2.controller.composite.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import objetosPractica2.controller.composite.ValidatorComposite;
import objetosPractica2.modelo.Circle;
import objetosPractica2.modelo.Figure;

public class ValidatorCompositeTest2 {
	Circle figTest; 
	
	
	@Before
	public void setUp() throws Exception{
		figTest = new Circle("Cir Double space", -10);
		
	}
	
	@After
	public void tearDown() throws Exception{
		figTest=null;
	}

	@Test
	public void testGetErroresNombreDobleEspacioyRadioNegativo() {
		assertEquals("El nombre no puede tener dos espacios\nEl radio debe ser mayor que 0 (cero)\n", ValidatorComposite.getErrors(figTest));
	}
	
	@Test
	public void testGetErroresNombreDobleEspacio() {
		figTest.setRadius(20);
		assertEquals("El nombre no puede tener dos espacios\n", ValidatorComposite.getErrors(figTest));
	}
	@Test
	public void testGetErroresRadioNegativo() {
		figTest.setName("circulo");
		assertEquals("El radio debe ser mayor que 0 (cero)\n", ValidatorComposite.getErrors(figTest));
	}
	
	
	
	
}
