package objetosPractica2.controller.composite;

import objetosPractica2.modelo.Rectangle;

public class RectBaseHeightNeg0Composite extends ValidatorComposite {

	@Override
	public String getError() {
		// TODO Auto-generated method stub
		return "height must be > 0";
	}

	@Override
	public boolean isMe() {
		// TODO Auto-generated method stub
		return figure instanceof Rectangle;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
		Rectangle rec = (Rectangle)figure;
		return rec.getHeight()<=0;
	}

}
