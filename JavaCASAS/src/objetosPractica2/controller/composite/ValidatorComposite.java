package objetosPractica2.controller.composite;

import java.util.ArrayList;
import java.util.List;

import objetosPractica2.modelo.Figure;

/**
 * @author nysg
 * esta clase agrupa todas las validaciones, es un patron composite por lo cual el padre conoce a todos sus hijos
//y devuelve la acumulaciónde todos los errores 
//@author NoyaSantiago
 */
public abstract class ValidatorComposite {

	
	static protected Figure figure; 
	
	
	public abstract String getError();
	
	
	public static String getErrors(Figure fig) {

		figure=fig;
		
		
		List<ValidatorComposite> validations = new ArrayList<>();
		validations.add(new FiguraNombreDobleEspacioComposite());
		validations.add(new FiguraNombreVacioComposite());
		validations.add(new CircleNegRad0Composite());
		validations.add(new SquareNegSide0Composite());
		validations.add(new RectBaseNeg0Composite());
		validations.add(new RectBaseHeightNeg0Composite());
		StringBuffer sbErrors = new StringBuffer();
		for (ValidatorComposite validation : validations) {
			
			if(validation.isMe() && validation.validate()) {
				sbErrors.append(validation.getError());
				sbErrors.append("\n");
				
			}
			
			
		}
		return sbErrors.toString(); 

		
		
	}
	
	public abstract boolean isMe();
	


	public abstract boolean validate();
	

	
	//Constructor
	public ValidatorComposite() {
	}


}
