package objetosPractica2.controller.composite;

import objetosPractica2.modelo.Square;

public class SquareNegSide0Composite extends ValidatorComposite {

	@Override
	public String getError() {
		// TODO Auto-generated method stub
		return "Side must be > 0";
	}

	@Override
	public boolean isMe() {
		// TODO Auto-generated method stub
		return figure instanceof Square;
	}

	@Override
	public boolean validate() {
		// TODO Auto-generated method stub
//		return false;

		Square squ = (Square)figure;
		return squ.getSide()<=0; 
	}




}
