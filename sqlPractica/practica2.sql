-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 1--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    ALU_NOMBRE,
    ALU_APELLIDO,
    ALU_DIRECCION
from
    alumnos 
-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 2--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    PROF_NOMBRE,
    PROF_APELLIDO,
    PROF_DNI,
    PROF_DIRECCION
from
    profesores
where
    PROF_NOMBRE != "nombre_test"
-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 3--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    MAT_NOMBRE
from
    materias
where
    MAT_NOMBRE != "Materia_test"
    AND MAT_NOMBRE != "materia 1_test" 

-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 4--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    MAT_NOMBRE,
    CUR_ANIO,
    CUR_DESCRIPCION
from
    materias,
    cursos --where MAT_NOMBRE != "Materia_test" AND MAT_NOMBRE != "materia 1_test" AND MAT_NOMBRE != "materia 1_test" AND CUR_ANIO != 2018 AND CUR_ANIO != 10
where
    materias.CUR_ID = cursos.CUR_ID
    AND MAT_NOMBRE != "Materia_test"
    AND MAT_NOMBRE != "materia 1_test"
    AND CUR_ANIO != 2018
    AND CUR_ANIO != 10
-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 5--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    ALU_NOMBRE,
    ALU_APELLIDO,
    PART_NOMBRE
from
    alumnos
    inner join partidos on alumnos.PART_ID = partidos.PART_ID
-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 6--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    alumnos.ALU_NOMBRE,
    alumnos.ALU_APELLIDO,
    alumnos.ALU_DNI,
    provincias.PROV_NOMBRE
from
    alumnos
    inner join provincias on alumnos.PROV_ID = provincias.PROV_ID 
-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 7--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
-- select
--     TUR_ID,
--     DIV_ID,
--     PROF_NOMBRE,
--     PROF_APELLIDO
-- FROM
--     comisiones
--     inner join profesores on comisiones.PROF_ID = profesores.PROF_ID
select 
    TUR_DESCRIPCION,
    DIV_DESCRIPCION,
    PROF_NOMBRE,
    PROF_APELLIDO
FROM
    turnos
    inner join comisiones on turnos.TUR_ID = comisiones.TUR_ID
    inner join division on comisiones.DIV_ID = division.DIV_ID
    inner join profesores on comisiones.PROF_ID = profesores.PROF_ID
-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 8--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    CUR_ANIO,
    CUR_DESCRIPCION,
    turnos.TUR_DESCRIPCION,
    DIV_DESCRIPCION,
    materias.MAT_NOMBRE
from
    cursos
    inner join comisiones on cursos.CUR_ID = comisiones.CUR_ID
    inner join turnos on comisiones.TUR_ID = turnos.TUR_ID
    inner join division on comisiones.DIV_ID = division.DIV_ID
    inner join materias on comisiones.MAT_ID = materias.MAT_ID
-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 9--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    MAT_NOMBRE,
    TEM_DESCRIPCION,
    ITEM_DESCRIPCION
from
    materias
    inner join temarios on materias.TEM_ID = temarios.TEM_ID
    inner join itemsdetemario on temarios.TEM_ID = itemsdetemario.TEM_ID

-----------------------------------------------------------
-----------------------------------------------------------
----------------------Ejercicio 10--------------------------
-----------------------------------------------------------
-----------------------------------------------------------
select
    MOD_DESCRIPCION,
    MAT_NOMBRE,
    CUR_ANIO,
    CUR_DESCRIPCION
from
    modulos
    inner join materias on modulos.MAT_ID = materias.MAT_ID
    inner join cursos on materias.CUR_ID = cursos.CUR_ID

select * from cursos
